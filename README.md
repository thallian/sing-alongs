Sing Alongs
===========

Motivation
----------

I love to play the tin whistle and around 2012 I was looking for some sheet
music. There are a lot of good resources out there but the one thing I have
never found was a collection which was free (as in freedom) and usable for
printing. And if something does not exist why not try and create it yourself?

At this point I discovered [Lilypond](http://www.lilypond.org/) a wonderful
project for music engraving. The best part is that it has support for embedding
its scores in LaTeX files so it is possible to get complex layouts without a
big hassle. As a programmer I instantly liked the approach because I did not
need a big editor but could just write text files which I then could manage
with something like [git](http://git-scm.com/).

Development
-----------

Take a look at the command list in the makefile to see what you need for building.

The makefile defines multiple targets:

- pdfs: builds all songs as single pdfs (without lyrics for now)
- midis: builds the midi file for each song
- opus: converts the midi files into opus files
- book: concatenates the songs (with lyrics) to a songbook
- booklet: converts the songbook to a booklet fit for double sided flip printing

To build all just run `make` and it takes care of everything.

License Stuff
-------------

I am still not quite clear on how this works. My understanding is that all these
songs already are in the public domain and there is no need to explicitly set a license.

All copyrights that could arise from other sides (makefile or whatever) are waived
under [CC0](https://creativecommons.org/publicdomain/zero/1.0/).

Contact
-------

You can reach me at [sebastian@vanwa.ch](mailto:sebastian@vanwa.ch)
