SHELL = /bin/sh

# a list of all used commands (they have to be in your PATH)
LILYPOND = lilypond
LUALATEX = lualatex
MAKEINDEX = makeindex
TIMIDITY = timidity
OPUSENC = opusenc
PDF2PS = pdf2ps
PSBOOK = psbook
PSTOPS = pstops
PSNUP = psnup
PS2PDF = ps2pdf
SED = sed # do not use in-place editing for compatibility between different sed versions
RM = rm
MKDIR = mkdir
CD = cd
CP = cp

SRCDIR = src
OUTDIR = out
PDFDIR = pdf
MIDIDIR = midi
OPUSDIR = music
SCORESDIR = $(SRCDIR)/scores
LYRICSDIR = $(SRCDIR)/lyrics

OUTPUTFILE = $(OUTDIR)/singalongs.tex
TEXFILES = $(wildcard $(SRCDIR)/*.tex)
SCORES = $(wildcard $(SCORESDIR)/*.ly)
LYRICS = $(wildcard $(LYRICSDIR)/*.tex)
PDFS = $(patsubst %.ly,$(PDFDIR)/%.pdf,$(notdir $(SCORES)))
MIDIS = $(patsubst %.ly,$(MIDIDIR)/%.midi,$(notdir $(SCORES)))
OPUSFILES = $(patsubst %.ly,$(OPUSDIR)/%.opus,$(notdir $(SCORES)))

LILYPONDSUBST = $(foreach score,$(SCORES), \\\\chapter {$(shell grep title "$(score)" | awk -F '"|"' '{print $$2}')} \\\\includely[staffsize=18]{$(abspath $(score))} ~\\\\\\\\ ~\\\\\\\\ \\\\input{$(abspath $(LYRICSDIR)/$(notdir $(basename $(score)))).tex})

BOOKPDF = $(PDFDIR)/book.pdf
BOOKLETPDF = $(PDFDIR)/booklet.pdf

MIDITEMPO = 120

all: pdfs midis opus book booklet

pdfs: $(PDFS)

midis: pdfs $(MIDIS)

opus: midis $(OPUSFILES)

book: $(BOOKPDF)

booklet: $(BOOKLETPDF)

$(BOOKPDF): $(TEXFILES) $(SCORES) $(LYRICS) | $(PDFDIR) $(OUTDIR)
	@$(CP) $(TEXFILES) $(OUTDIR)/
	@$(CP) -r lib/* $(OUTDIR)/
	@$(CP) -r $(SRCDIR)/images $(OUTDIR)/images

	@$(SED) "s;\musicbooklet;$(LILYPONDSUBST);g" $(SRCDIR)/singalongs.tex > $(OUTPUTFILE).00
	@$(SED) 's/REVISION/$(shell git log -1 --format="%h")/g' $(OUTPUTFILE).00 > $(OUTPUTFILE)

	@$(CD) $(OUTDIR); \
	$(LUALATEX) -shell-escape singalongs.tex; \
	$(MAKEINDEX) singalongs.tex; \
	$(LUALATEX) -shell-escape singalongs.tex

	@$(CP) $(OUTDIR)/singalongs.pdf $(PDFDIR)/$(@F)

$(BOOKLETPDF): $(BOOKPDF)
	$(PDF2PS) $(BOOKPDF) - | $(PSBOOK) | $(PSTOPS) -pa4 '1:0@1.0(-1.25cm,0cm)' | $(PSNUP) -2 | $(PS2PDF) - $(PDFDIR)/$(@F)

$(PDFDIR)/%.pdf: $(SCORESDIR)/%.ly | $(PDFDIR) $(OUTDIR)
	@$(SED) 's/%title/title/g' $< > $(OUTDIR)/$(notdir $<).00
	@$(SED) 's/\header {/\paper{indent=0\\mm} \\header { tagline=""/g' $(OUTDIR)/$(notdir $<).00 > $(OUTDIR)/$(notdir $<)
	@$(LILYPOND) --output=$(PDFDIR)/$(basename $(@F)) $(OUTDIR)/$(notdir $<)

$(MIDIDIR)/%.midi: $(SCORESDIR)/%.ly | $(MIDIDIR) $(OUTDIR)
	@$(SED) 's/\layout { }/\midi{ \\tempo 4 = $(MIDITEMPO) }/g' $< > $(OUTDIR)/$(notdir $<).00
	@$(SED) 's/\header/\include "articulate.ly" \\header/g' $(OUTDIR)/$(notdir $<).00 > $(OUTDIR)/$(notdir $<).01
	@$(SED) 's/<</\\articulate \\unfoldRepeats <</g' $(OUTDIR)/$(notdir $<).01 > $(OUTDIR)/$(notdir $<)
	@$(LILYPOND) --output=$(MIDIDIR)/$(basename $(@F)) $(OUTDIR)/$(notdir $<)

$(OPUSDIR)/%.opus: $(MIDIDIR)/%.midi | $(OPUSDIR)
	@$(TIMIDITY) $< -Ow -o - | $(OPUSENC) - $(OPUSDIR)/$(@F)

$(PDFDIR):
	@$(MKDIR) -p $(PDFDIR)

$(MIDIDIR):
	@$(MKDIR) -p $(MIDIDIR)

$(OPUSDIR):
	@$(MKDIR) -p $(OPUSDIR)

$(OUTDIR):
	@$(MKDIR) -p $(OUTDIR)

.PHONY: clean
clean:
	@$(RM) -rf $(PDFDIR) $(MIDIDIR) $(OPUSDIR) $(OUTDIR)
