﻿\version "2.16.0"

\header {
  %title = "Tim Finnegan's Wake"
  composer = "Traditional"
}

global = {
  \time 2/4
  \key g \major
}

chordNames = \chordmode {
  \global
  s8 g2 e:m c d g e:m c d4 g1*3/4 e2:m c d g e:m c d4 g1*3/4 e2:m c d g e:m c d4 g8  
}

melody = \relative a' {
  \global
  \partial 8 a8 | 
  b16 b b8 b a | 
  b e e8. fis16 |
  g g fis8 e d | 
  b a a a16 a | 
  b b b8 b a |
  b e e fis16 fis | 
  g8 fis16 fis e8 d | 
  e16 e fis8 g8. d16 |
  g8 g16 g g8 a16 a | 
  g8 fis e d16 d | 
  g8 g16 g g g a8 |
  g fis e8. d16 | 
  g8 g g a16 a | 
  g8 fis e d16 d |    
  e8 e16 e e8 d | 
  e fis g4 | 
  b,8 b16 b b8 a |
  b e16 e e8 fis | 
  g fis e d | 
  b16 a8. a4 |
  b8 b b a | 
  b e e fis | 
  g fis e d |
  e16 e fis8 g \bar "|." 
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff { \melody }
  >>
  \layout { }
}
