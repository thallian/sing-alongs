﻿\version "2.16.0"

\header {
  %title = "The Irish Rover"
  composer = "Traditional"
}

global = {
  \time 4/4
  \key g \major
}

chordNames = \chordmode {
  \global
  s4 g1 c g d g2 d g1*2 d1 g d g e:m g2 d g1 
}

melody = \relative d'' {
  \global
  \partial 4 d8 b |
   
  \repeat volta 2 
  {        
    g4 b8 c d4 g8 a |
    b4 a8 fis g4 fis8 e |        
  }    
  \alternative 
  {
    { d4 e8 c b4 c8 b | a2. b8 a }     
    { d4 fis8 g a4 fis8 d | g2. d8 d } 
  }
    
  g8 g4 a8 b4 a8 g |
  a a4 fis8 d4. d8 |
  g4. a8 b4 a8 g |
  a4. fis8 d c b a |
  g4 b8 c d4 g8 a |
  b4 a8 fis g4 fis8 e |
  d4 fis8 g a4 fis8 d | 
  g2. s4 \bar "|." 
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff { \melody }
  >>
  \layout { }
}
