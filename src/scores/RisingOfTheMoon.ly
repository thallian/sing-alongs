﻿\version "2.16.0"

\header {
  %title = "The Rising of the Moon"
  composer = "John Keegan Casey"
}

global = {
  \time 4/4
  \key g \major
}

chordNames = \chordmode {
  \global
  s4 g1 d c2 g d g  
}

melody = \relative d' {
  \global
  \partial 4 g8 a | 
  b8. b16 b8 b b d4 b8 | 
  b a a b a4. d8 |
  e8. c16 g'8 fis e d b g | 
  a8. a16 g8 fis g4 r4 \bar "|."  
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff { \melody }
  >>
  \layout { }
}
