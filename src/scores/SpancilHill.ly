\version "2.16.2"

\header {
  %title = "Spancil Hill"
  composer = "Michael Considine"
}

global = {
  \time 3/4
  \key g \major
}

chordNames = \chordmode {
  \global
  s4 
  a1*3/2:m 
  g 
  e:m 
  a1*9/2:m 
  g1*9/4 
  e1*3/4:m 
  a1*3:m 
  g1*9/4 
  e1*3/4:m
  a1*3/2:m
  g
  e:m
  a:m  
}

melody = \relative e'' {
  \global
  \partial 4 e4 |
  e2 a,4 |
  e'2 e4 |
  d2 b4 |
  g2 a4 |
  b d b |
  a2 g4 |
  a2.( |
  a2) e'4 |
  e2 a4 |
  a2 b4 |
  a2 g4 |
  e2 fis4 |
  g2 a4 |
  g fis e |
  d2.( |
  d2) e4 |
  e2 a4 |
  a2 b4 |
  a2 g4 |
  e2 fis4 |
  g2 a4 |
  g fis e |
  d2.( |
  d4) g fis |
  e2 a,4 | 
  e'2 e4 |
  d2 b4 |
  g2 a4 |
  b d b |
  a2 g4 |
  a2.( |
  a2) s4 \bar "|."  
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff { \melody }
  >>
  \layout { }
}
