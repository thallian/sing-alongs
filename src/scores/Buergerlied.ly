﻿\version "2.16.0"

\header {
  %title = "Bürgerlied"
  composer = "Adalbert Harnisch"
}

global = {
  \time 5/4
  \key g \major
}

chordNames = \chordmode {
  \global
  s4 g1*3/4 e2:m g1*3/4 c2:7 a2:m d1*3/4:7 g1*3/4 c2:7 a2:m d1*3/4:7 g2 d4 g2 g d4 g2  
}

melody = \relative d' {
  \global
  \partial 4 d8 d |
  g4. g8 g a b b g a |
  b4 d d8 c c b a g |
  a b c b a4. r8 d c |
      
  \repeat volta 2
  {
    b4 d e8 d c b a g |
    a4 c d8 c b a g g |
  }
  \alternative
  {
    { g4 b a8 a g4 d'8 c }
    { g4 b a8 a g4 s4 \bar "|." }
  } 
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff { \melody }
  >>
  \layout { }
}
