﻿\version "2.16.0"

\header {
  %title = "Molly Malone"
  composer = "Traditional"
}

global = {
  \time 3/4
  \key g \major
}

chordNames = \chordmode {
  \global
  s4 c1*6/4 g c g g1*3/4 c 
}

melody = \relative g' {
  \global
  \partial 4 d4 |
  \repeat volta 3 
  {
    g g g |
    g8 b4. a8 g |
    a4 a a |
    a8 c4. b8 a |
    d4 b g |
    d' b g |      
  }
  \alternative 
  {
    {
      \set Score.repeatCommands = #'((volta "1"))        
      e c' b |
      a2 d,4 |
    }
    { 
      \set Score.repeatCommands = #'((volta "2-3") end-repeat)        
      a'4. g8 a4 |
      g2 d4 \bar "|."
    }      
  }    
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff { \melody }
  >>
  \layout { }
}
