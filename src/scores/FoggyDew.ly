﻿\version "2.16.0"

\header {
  %title = "The Foggy Dew"
  composer = "Charles O’Neill"
}

global = {
  \time 4/4
  \key g \major
}

chordNames = \chordmode {
  \global
  s2 a1*2:m g c1 a1*2:m s1 c1*2 g1 e:m c a1*3:m a1*2:m g1*2 c1 a:m  
}

melody = \relative g' {
  \global
  \partial 2 b4 d | 
  e2 d4 b | 
  e2 d4 b |
  a2 b | 
  d, e4 fis | 
  g b a g |
  e2. d4 | 
  e1( | 
  e2) \bar ":|"
  \partial 2 fis | 
  g b | 
  d c4 b |
  a2 a | 
  b g4 a | 
  b2 g'4 fis |
  e d b d | 
  e1( | 
  e2) b4 d |
  e2 d4 b | 
  e2 d4 b | 
  a2 b |
  d, e4 fis | 
  g b a g | 
  e2. e4 |
  e2 \bar "|." 
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff { \melody }
  >>
  \layout { }
}
