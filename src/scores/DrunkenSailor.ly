﻿\version "2.16.0"

\header {
  %title = "Drunken Sailor"
  composer = "Traditional"
}

global = {
  \time 2/4
  \key d \major
}

chordNames = \chordmode {
  \global
  a1:m g a:m g2 a:m a1:m g a:m g2 a:m  
}

melody = \relative b' {
  \global
  b8 b16 b b8 b16 b | 
  b8 e, g b | 
  a a16 a a8 a16 a |
  a8 d, fis a | 
  b b16 b b8 b16 b | 
  b8 cis d e |
  d b a fis |
  e4 e \bar ":|:"
  b' b | 
  b8 e, g b | 
  a4 a | 
  a8 d, fis a | 
  b4 b | 
  b8 cis d e | 
  d b a fis | 
  e4 e  \bar ":|" 
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff { \melody }
  >>
  \layout { }
}
