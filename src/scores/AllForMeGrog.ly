﻿\version "2.16.2"

\header {
  %title = "All for Me Grog"
  composer = "Stephen Hatfield"
}

global = {
  \time 4/4
  \key g \major
}

chordNames = \chordmode {
  \global
  s8 g1 c2 g1. d1 g1 c2 g1 d2 d2:7 g4.
}

melody = \relative d' {
  \global
  \partial 8 d8 |
   g4 b8. a16 g4. fis8 |
   e fis g e d2 |
   d'4 d8. d16 d4 c8 b |
   b a4.( a4) b8 c |
   d(e) d b g4 g8 fis |
   e a g e d4 g8 a |
   b d c b b a g fis |
   a2 g4( g8) \bar "|."
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff { \melody }
  >>
  \layout { }
}
