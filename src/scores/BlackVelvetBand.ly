﻿\version "2.16.0"

\header {
  %title = "The Black Velvet Band"
  composer = "Traditional"
}

global = {
  \time 3/4
  \key g \major
}

chordNames = \chordmode {
  \global
  s4 d1.*3 a1.:7 d1*9/4 a d  
}

melody = \relative g'' {
  \global
  \partial 4 d4 | 
  d2 d4 | 
  b c4. d8 |
  c4 b2( | 
  b) a4 | 
  g a b |
  g fis e | 
  d2.( | 
  d4) d' c |
  b2 b4 | 
  d, e fis | 
  g2( a4) | 
  b2 g4 | 
  a b c | 
  fis, g a |
  g2.( | 
  g2) s4 \bar "|."  
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff { \melody }
  >>
  \layout { }
}
