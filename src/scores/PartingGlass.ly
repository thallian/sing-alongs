﻿\version "2.16.0"

\header {
  %title = "The Parting Glass"
  composer = "Traditional"
}

global = {
  \time 4/4
  \key g \major
}

chordNames = \chordmode {
  \global
  s4 e1:m d e:m d e:m d e2:m d e1:m g1*2 a1:m g e:m d e2:m d e1:m  
}

melody = \relative b' {
  \global
  \partial 4 b8 (a) | 
  g4 e e d8 e | 
  g4 g a g8 (a) |
  b4 b b8 (a) g (a) | 
  b4 d, d b'8 (a) | 
  g4 e e d8 (e) |
  g4 g a g8 (a) | 
  b4 e d8 (b) a (b) | 
  g4 e e4. b'8 |
  d (b) d (e) d4. b8 | 
  d (b) d (e) d4. b8 | 
  c4 b a g8 (a) |
  b4 d, d b'8 (a) | 
  g4 e e d8 (e) | 
  g4 g a g8 (a) |
  b4 e d8 (b) a (b) | 
  g4 e e s \bar "|."  
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff { \melody }
  >>
  \layout { }
}
