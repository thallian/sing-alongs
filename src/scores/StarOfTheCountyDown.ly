﻿\version "2.16.0"

\header {
  %title = "The Star of the County Down"
  composer = "Cathal Mac Garvey"
}

global = {
  \time 4/4
  \key g \major
}

chordNames = \chordmode {
  \global
  s4 e1:m g2 d e1:m d e:m g2 d e2:m d e1:m g d e:m d e:m g2 d e:m d e1:m
}

melody = \relative c' {
  \global
  \partial 4 fis8 a | 
  b4 b b a8 b | 
  d4 d e d8 e |
  fis4 e8 d b4 a8 fis | 
  a2. fis8 a | 
  b4 b b a8 b |
  d4 d e d8 e | 
  fis4 e8 d b4 b | 
  b2. |
  \repeat volta 2 
  {
    \partial 4 e8 fis |
    a4 fis fis e8 d | 
    e4 e e d8 e |
    fis4 e8 d b4 a8 fis | 
    a2. fis8 a | 
    b4 b b a8 b |
    d4 d e d8 e | 
    fis4 e8 d8 b4 b | 
    b2. s4 \bar "|."    
  }  
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff { \melody }
  >>
  \layout { }
}
