﻿\version "2.16.0"

\header {
  %title = "Muirsheen Durkin"
  composer = "Traditional"
}

global = {
  \time 4/4
  \key d \major
}

chordNames = \chordmode {
  \global
  s4 g1 d1*2 g d g d g d g1
}

melody = \relative d' {
  \global
  \partial 4 d8. e16 | 
  fis4 fis e d | 
  e a a b |
  cis a g e | 
  e d2 d8. e16 | 
  fis4 fis e d | 
  e a2 b4 | 
  cis8 cis a4 b cis |
  d2. a4 | 
  d d e d | 
  cis a a b | 
  cis a g e | 
  e d2 d8. e16 |
  fis2 e4 d | 
  e a a b |
  cis a g( e) | 
  d2. s4 \bar "|." 
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff { \melody }
  >>
  \layout { }
}
