﻿\version "2.16.0"

\header {
  %title = "Auld Lang Syne"
  composer = "Robert Burns"
}

global = {
  \time 4/4
  \key d \major
}

chordNames = \chordmode {
  \global
  s4 d1 a:7 d g d a2.:7 fis4 b2:m e4:min7 a:7 d1 d1 a:7 d g d a2.:7 fis4 b2:m e4:min7 a:7 d1  
}

melody = \relative a' {
  \global
  \partial 4 a4 |      
  d4. cis8 d4 fis |
  e4. d8 e4 fis |
  d4. d8 fis4 a |
  b2. b4 |
  a4. fis8 fis4 d |
  e4. d8 e4 fis8 e |
  d4. b8 b4 a |
  d2. b'4 |
  a4. fis8 fis4 d |
  e4. d8 e4 b' |
  a4. fis8 fis4 a |
  b2. b4 |
  a4. fis8 fis4 d |
  e4. d8 e4 fis8 e |
  d4. b8 b4 a |
  d2. s4 \bar "|."  
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff { \melody }
  >>
  \layout { }
}
