﻿\version "2.16.0"

\header {
  %title = "The Wild Rover"
  composer = "Traditional"
}

global = {
  \time 6/8
  \key g \major
}

chordNames = \chordmode {
  \global
  s8 g1*9/8 c1*3/8 g c d:7 g d:7 g d1*3/2:7 g1*3/4 c g c d:7 g  
}

melody = \relative g' {
  \global
  \partial 8 g8 |
  
  \repeat volta 2
  { 
    g8. a16 g8 e d b' |           
    b8. a16 b8 c4 b16 c |
    d8 b d c a fis | 
  }
  \alternative
  {
    { d b' a g4 g8 }
    { d b' a g fis g }
  }     
   
  a4. a |      
  fis8 d4( d4.)( | 
  d8) b' b b a b | 
  c4.( c8) b c | 
  d4.( d8) b g | 
  fis e4( e) e8 | 
  d8 b'4( b) a8 | 
  g2. \bar "|."  
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff { \melody }
  >>
  \layout { }
}
