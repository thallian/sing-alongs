﻿\version "2.16.0"

\header {
  %title = "The Kerry Recruit"
  composer = "Traditional"
}

global = {
  \time 6/8
  \key g \major
}

chordNames = \chordmode {
  \global
  s8 g1*6/8 d:7 d c1*3/8 g  
}

melody = \relative b' {
  \global
  \partial 8 b8 | 
  a8 g g g4 b8 |   
  b a a a4 d8 |
  e d d b4 a8 |
  a g g g4 \bar "|."  
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff { \melody }
  >>
  \layout { }
}
