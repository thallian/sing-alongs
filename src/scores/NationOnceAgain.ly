﻿\version "2.16.0"

\header {
  %title = "A Nation Once Again"
  composer = "Thomas Osborne Davis"
}

global = {
  \time 4/4
  \key d \major
}

chordNames = \chordmode {
  \global
  s4 g1*2 c2 d g1*3 c2 a:m d1*3 c2 g1*6/4 c1 g2 d g d g1*2 c1 a:m d g c2 d g d g1  
}

melody = \relative a' {
  \global
  \partial 4 a4 | 
  a b8 cis d4 cis8 d | 
  fis4. e8 d4. cis8 |
  b4 e d8 cis b cis | 
  e4 d d2 | 
  a4 b8 cis d4 cis8 d |
  fis4. e8 d4. cis8 | 
  b4 e d8 cis b cis | 
  b4 a a2 |
  a4 b cis d | 
  e4. a,8 a2 | 
  b4 cis d e | 
  fis2 fis8 e d cis | 
  b4 b g'4. fis8 | 
  e4 d cis4. b8 |
  a fis'4. e d8 | 
  d2 d4 a | 
  fis'4. e8 d4. cis8 | 
  b2 b4 b | 
  g'4. fis8 e4. d8 | 
  cis2 cis4 b |
  a4 b8 cis d4 cis8 d |
  b4 e8 d cis4 d8 e |
  fis g4. e d8 |
  d2. s4 \bar "|." 
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff { \melody }
  >>
  \layout { }
}
