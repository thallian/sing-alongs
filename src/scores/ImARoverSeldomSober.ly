﻿\version "2.16.0"

\header {
  %title = "I'm a Rover Seldom Sober"
  composer = "Traditional"
}

global = {
  \time 4/4
  \key g \major
}

chordNames = \chordmode {
  \global
  s4 g2 c g1*5/4 d1*3/4 g1*2 d4 g2  
}

melody = \relative g' {
  \global
  \partial 4 g8. g16 |
  g4 d8. d16 e8. e16 e4 |
  d g8. g16 g4 b8. b16 |
  b8. b16 a4 r8 d8 d8. c16 |
  b4 d8. d16 a8. fis16 g4 d |
  g8. a16 b4 b8. b16 a8. |
  a16 g4 r4 \bar "|."  
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff { \melody }
  >>
  \layout { }
}
