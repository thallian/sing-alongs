﻿\version "2.16.0"

\header {
  %title = "Johnny I hardly Knew Ye"
  composer = "Traditional"
}

global = {
  \time 6/8
  \key g \major
}

chordNames = \chordmode {
  \global
  s8 e1*6/8:m c g1*12/8 e1*6/8:m c g1*12/8 g1*6/8 d1*6/8 c b:7 e1*3/8:m d c b:7 e1*12/8:m  
}

melody = \relative c' {
  \global
  \partial 8 e8 | 
  e a a a4 b 8 | 
  c4 b8 c4 a8 |
  g2 ( g8) e | 
  g2 ( g8) a | 
  e a a a4 b8 |
  c4 b8 c4 d8 | 
  e2( e8) c8 | 
  e2( e8) c8 |
  e4 e8 e d c | 
  d4 d8 d4 b8 | 
  c4 c8 c b a |
  b4 b8 b c d | 
  e4( e8) d4( d8) | 
  c4( c8) b4( e,8) |
  e a a a4 g8 | 
  a4.( a4) r8 \bar "|." 
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff { \melody }
  >>
  \layout { }
}
