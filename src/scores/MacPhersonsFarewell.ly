﻿\version "2.16.0"

\header {
  %title = "MacPherson's Farewell"
  composer = "Robert Burns"
}

global = {
  \time 4/4
  \key g \major
}

chordNames = \chordmode {
  \global
  s4 g1 d g c g d g c g1 d g c g d g1*3/4 c4 g1 
}

melody = \relative g' {
  \global
  \partial 4 d4 | 
  g4. a8 b4 a8 ( g)| 
  a ( g) a (b) a4 b8 ( a) |
  g4. a8 b4 a8( g) | 
  e2. d8( e) | 
  g4. a8 b4 a8( g) |
  a ( g) a (b) a4 b8 ( a) |
  g4. b8 a8( g) e4 | 
  d2. b'8( c) | 
  d4. b8 c( b) a( g) | 
  b4 a a b8( c) |
  d4. b8 c( b) a( g) | 
  e2. e'4 | 
  d4. b8 c( b) a g |
  b4 a a d, | 
  g4. b8 a( g) e4 | 
  g2. s4 \bar "|." 
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff { \melody }
  >>
  \layout { }
}
