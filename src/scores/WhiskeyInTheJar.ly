﻿\version "2.16.0"

\header {
  %title = "Whiskey in the Jar"
  composer = "Traditional"
}

global = {
  \time 4/4
  \key d \major
}

chordNames = \chordmode {
  \global
  s4 g1*2 e:m c g1 d g d1*2 g c1*2 g2 d g1 
}

melody = \relative fis' {
  \global
  \partial 4 fis4 | 
  
  \repeat volta 2
  {
    a4. a8 a4 b | 
    a fis2 a4 |
    b4. b8 b4 cis | 
    b fis2 a4 | 
    b4. b8 b4 cis |
    d2 cis4 b | 
    a8 b4 d4. cis4 |
  }
  \alternative
  {
    { b fis2 fis4 }
    { b a fis d }
  }
  
  e e8 e e4 fis |
  e1 | 
  r4 fis fis4. e8 | 
  fis8 a4. a2 |
  r4 b b4. a8 | 
  b d4. d4 b | 
  a fis e fis | 
  d2. s4 \bar "|." 
}

\score {
  <<
    \new ChordNames \chordNames
    \new Staff { \melody }
  >>
  \layout { }
}
